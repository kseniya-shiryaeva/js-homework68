import Counter from "./containers/Counter/Counter";
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";
import Todo from "./containers/Todo/Todo";
import './App.css';

const App = () => {
  return (
      <div className="App">
          <BrowserRouter>
            <nav>
                <NavLink to="/">Counter</NavLink>
                <NavLink to="/todo">To do list</NavLink>
            </nav>
            <div className="content">
              <Switch>
                  <Route path="/" exact component={Counter}/>
                  <Route path="/todo" exact component={Todo}/>
              </Switch>
            </div>

        </BrowserRouter>
      </div>
  );
};

export default App;
