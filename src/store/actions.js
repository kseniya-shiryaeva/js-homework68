import axios from 'axios';

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const FETCH_TASKS_REQUEST = 'FETCH_TASKS_REQUEST';
export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS';
export const FETCH_TASKS_FAILURE = 'FETCH_TASKS_FAILURE';

export const CHANGE_TASK_TEXT = 'CHANGE_TASK_TEXT';
export const DELETE_TASK = 'DELETE_TASK';

export const increase = () => ({type: INCREASE});
export const decrease = () => ({type: DECREASE});
export const add = value => ({type: ADD, payload: value});
export const subtract = value => ({type: SUBTRACT, payload: value});

export const changeTaskText = value => ({type: CHANGE_TASK_TEXT, payload: value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload: counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const fetchTasksRequest = () => ({type: FETCH_TASKS_REQUEST});
export const fetchTasksSuccess = tasks => ({type: FETCH_TASKS_SUCCESS, payload: tasks});
export const fetchTasksFailure = () => ({type: FETCH_TASKS_FAILURE});

export const fetchCounter = () => {
  return async (dispatch, getState) => {
    dispatch(fetchCounterRequest());

    try {
      const response = await axios.get('https://burger-kit-naviu-default-rtdb.europe-west1.firebasedatabase.app/counter.json');

      if (response.data === null) {
        dispatch(fetchCounterSuccess(0));
      } else {
        dispatch(fetchCounterSuccess(response.data));
      }
    } catch (e) {
      dispatch(fetchCounterFailure());
    }
  };
};

export const saveCounter = () => {
  return async (dispatch, getState) => {
    const currentCounter = getState().counter;
    await axios.put('https://burger-kit-naviu-default-rtdb.europe-west1.firebasedatabase.app/counter.json', currentCounter);
  };
};

export const fetchTasks = () => {
  return async (dispatch, getState) => {
    dispatch(fetchTasksRequest());

    try {
      const response = await axios.get('https://burger-kit-naviu-default-rtdb.europe-west1.firebasedatabase.app/todo.json');

      if (response.data === null) {
        dispatch(fetchTasksSuccess([]));
      } else {
        dispatch(fetchTasksSuccess(response.data));
      }
    } catch (e) {
      dispatch(fetchTasksFailure());
    }
  };
};

export const addTask = e => {
  e.preventDefault();
  return async (dispatch, getState) => {
    const newTask = getState().currentTask;
    await axios.post('https://burger-kit-naviu-default-rtdb.europe-west1.firebasedatabase.app/todo.json', {"text": newTask, isDone: false});
  };
};

export const makeTaskDone = id => {
  return async (dispatch, getState) => {
    const taskIsDone = getState().tasks[id].isDone;
    await axios.put('https://burger-kit-naviu-default-rtdb.europe-west1.firebasedatabase.app/todo/' + id + 'isDone.json', taskIsDone);
  };
};

export const removeTask = id => {
  return async (dispatch, getState) => {
    await axios.delete('https://burger-kit-naviu-default-rtdb.europe-west1.firebasedatabase.app/todo/' + id + '.json');
  };
};