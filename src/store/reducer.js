import {
  ADD,
  DECREASE,
  FETCH_COUNTER_FAILURE,
  FETCH_COUNTER_REQUEST,
  FETCH_COUNTER_SUCCESS,
  FETCH_TASKS_FAILURE,
  FETCH_TASKS_REQUEST,
  FETCH_TASKS_SUCCESS,
  CHANGE_TASK_TEXT,
  INCREASE,
  SUBTRACT
} from "./actions";

const initialState = {
  counter: 0,
  loading: false,
  error: null,
  tasks: [],
  currentTask: ''
};

const add = (state, action) => {
  return {...state, counter: state.counter + action.payload};
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREASE:
      return {...state, counter: state.counter + 1};
    case DECREASE:
      return {...state, counter: state.counter - 1};
    case ADD:
      return add(state, action);
    case SUBTRACT:
      return {...state, counter: state.counter - action.payload};
    case FETCH_COUNTER_REQUEST:
      return {...state, error: null, loading: true};
    case FETCH_COUNTER_SUCCESS:
      return {...state, loading: false, counter: action.payload};
    case FETCH_COUNTER_FAILURE:
      return {...state, loading: false, error: action.payload};
    case FETCH_TASKS_REQUEST:
      return {...state, error: null, loading: true};
    case FETCH_TASKS_SUCCESS:
      return {...state, loading: false, tasks: action.payload};
    case FETCH_TASKS_FAILURE:
      return {...state, loading: false, error: action.payload};
    case CHANGE_TASK_TEXT:
      return {...state, currentTask: action.payload};
    default:
      return state;
  }
}

export default reducer;