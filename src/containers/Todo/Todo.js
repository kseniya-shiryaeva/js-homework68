import React, {useEffect} from 'react';
import AddTaskForm from "../../components/AddTaskForm/AddTaskForm";
import Task from "../../components/Task/Task";
import './Todo.css';
import {addTask, changeTaskText, fetchTasks, makeTaskDone} from "../../store/actions";
import {useDispatch, useSelector} from "react-redux";

const Todo = () => {
    const dispatch = useDispatch();
    const tasks = useSelector(state => state.tasks);
    const currentTask = useSelector(state => state.currentTask);

    useEffect(() => {
        dispatch(fetchTasks());
        console.log(tasks);
    }, [dispatch]);

    const deleteTask = id => {
        dispatch(deleteTask(id));
    };

    const toggleTaskDone = id => {
        dispatch(makeTaskDone(id));
    };

    const submitForm = (e) => {
        dispatch(addTask(e));
    };

    const changeText = (e) => {
        console.log(e.target.value);
        dispatch(changeTaskText(e.target.value));
    }

    let taskComponents = null;

    if (Object.keys(tasks).length > 0) {
        taskComponents = Object.keys(tasks).map(id => (
            <Task
                key={id}
                text={tasks[id].text}
                id={tasks[id].id}
                onDelete={() => deleteTask(tasks[id].id)}
                onToggleDone={() => toggleTaskDone(tasks[id].id)}
            >
            </Task>
        ));
    }

    return (
        <div className="Todo container">
            <AddTaskForm
                currentTask={currentTask}
                onSubmit={e => submitForm(e)}
                onChange={e => changeText(e)}
            >
            </AddTaskForm>
            <div className="task-container">
                {taskComponents}
            </div>
        </div>
    );
};

export default Todo;